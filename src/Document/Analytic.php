<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;


/**
 *
 * @MongoDB\Document
 */
class Analytic
{
    /**
     * @MongoDB\Id(strategy="INCREMENT", type="integer")
     */
    private $id;

    /**
     * @MongoDB\Field(type="string")
     */
    private $version;

    /**
     * @MongoDB\Field(type="string")
     */
    private $hitType;

    /**
     * @MongoDB\Field(type="string")
     */
    private $documentLocation;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     */
    public function setVersion($version): void
    {
        $this->version = $version;
    }

    /**
     * @return mixed
     */
    public function getHitType()
    {
        return $this->hitType;
    }

    /**
     * @param mixed $hitType
     */
    public function setHitType($hitType): void
    {
        $this->hitType = $hitType;
    }

    /**
     * @return mixed
     */
    public function getDocumentLocation()
    {
        return $this->documentLocation;
    }

    /**
     * @param mixed $documentLocation
     */
    public function setDocumentLocation($documentLocation): void
    {
        $this->documentLocation = $documentLocation;
    }


}
